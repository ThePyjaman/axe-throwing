import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {IntlProvider} from "react-intl";

import messages_fr from "./translation/fr.json";
import messages_en from "./translation/en.json";

const language = navigator.language.split(/[-_]/)[0]

const messages = {
    'fr': messages_fr,
    'en': messages_en,
}

ReactDOM.render(
    <IntlProvider locale={language} messages={messages[language]}>
        <React.StrictMode>
            <App />
        </React.StrictMode>
    </IntlProvider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
