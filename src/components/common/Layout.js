import React, { useState } from 'react';
import NavBar from './NavBar';
import Main from './Main';

function Layout() {

    const [toggled, setToggled] = useState(false);

    const handleToggleSidebar = (value) => {
        setToggled(value);
    };

    return (
        <div className={`app ${toggled ? 'toggled' : ''}`}>
            <NavBar
                toggled={toggled}
                handleToggleSidebar={handleToggleSidebar}
            />
            <Main
                toggled={toggled}
                handleToggleSidebar={handleToggleSidebar}
            />
        </div>
    );
}
export default Layout;