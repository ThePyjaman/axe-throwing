import React from "react";
import {Menu, MenuItem, ProSidebar, SidebarContent, SidebarFooter} from "react-pro-sidebar";
import 'react-pro-sidebar/dist/css/styles.css';
import {useIntl} from 'react-intl'
import {BsCalendar, GiAxeSword, GiPartyHat, IoFastFood} from "react-icons/all";

const NavBar = ({ collapsed, rtl, toggled, handleToggleSidebar}) => {
    const intl = useIntl();

    return (
        <ProSidebar
            rtl={rtl}
            collapsed={collapsed}
            toggled={toggled}
            breakPoint="md"
            onToggle={handleToggleSidebar}
        >
            <SidebarContent>
                <Menu iconShape="circle">
                    <MenuItem icon={<GiAxeSword/>}>
                        {intl.formatMessage({id: 'gameModes'})}
                    </MenuItem>
                    <MenuItem icon={<IoFastFood/>}>
                        {intl.formatMessage({id: 'menu'})}
                    </MenuItem>
                    <MenuItem icon={<BsCalendar/>}>
                        {intl.formatMessage({id: 'calendar'})}
                    </MenuItem>
                    <MenuItem icon={<GiPartyHat/>}>
                        {intl.formatMessage({id: 'events'})}
                    </MenuItem>
                </Menu>
            </SidebarContent>

            <SidebarFooter style={{textAlign: 'center'}}>

            </SidebarFooter>
        </ProSidebar>
    );
}
export default NavBar