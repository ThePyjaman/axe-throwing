/** @jsxImportSource @emotion/react */
import React, { useState, useEffect, useRef} from "react";
import {css} from '@emotion/core'
import SliderContent from './SliderContent'
import Slide from './Slide' //
import Arrow from "./Arrow"; // les fleches qui permettent de swipent entre les slides
import Dots from "./Dots";


const getWidth = () => window.innerWidth

/**
 * @function Slider
 */
const Slider = props => {
    const  {slides, autoPlay} = props


    const firstSlide = slides[0]
    const secondSlide = slides[1]
    const lastSlide = slides[slides.length - 1]

    const [state, setState] = useState({
        activeSlide: 0,
        translate: getWidth(),
        transition: 0.45,
        _slides: [lastSlide, firstSlide, secondSlide]
    })

    const { activeSlide, translate, _slides, transition } = state

    const autoPlayRef = useRef(null)
    const transitionRef = useRef(null)
    const resizeRef = useRef(null)
    const sliderRef = useRef(null)

    useEffect(() => {
        autoPlayRef.current = nextSlide
        transitionRef.current = smoothTransition
        resizeRef.current = handleResize
    })


    console.log('aaaaaaaaaaaaaa')
    useEffect(() => {



        const play = () => {
            if(autoPlayRef.current){
                autoPlayRef.current()
            }
        }

        const smooth = e => {

                transitionRef.current()

        }

        const resize = () => {
            if(resizeRef.current){
                resizeRef.current()
            }
        }

        const slider = sliderRef.current
        let transitionEnd = null


        if(slider)
        {
            transitionEnd = slider.addEventListener('transitionend', smooth)
        }



        const onResize = window.addEventListener('resize', resize)

        let interval = null

        if (autoPlay) {
            //TODO reset timer on cLick
            interval = setInterval(play, autoPlay * 5000)
        }

        return () => {
            if(slider){
                console.log('oue bo jeu')
                slider.removeEventListener('transitionend', transitionEnd)
            }
            window.removeEventListener('resize', onResize)

            if (autoPlay) {
                clearInterval(interval)
            }
        }
    }, [autoPlay, autoPlayRef])

    useEffect(() => {
        if (transition === 0) setState({ ...state, transition: 0.45 })
    }, [transition, state])

    const handleResize = () => {
        setState({ ...state, translate: getWidth(), transition: 0 })
    }

    const smoothTransition = () => {
        let _slides = []

        if (activeSlide === slides.length - 1)
            _slides = [slides[slides.length - 2], lastSlide, firstSlide]
        else if (activeSlide === 0) _slides = [lastSlide, firstSlide, secondSlide]
        else _slides = slides.slice(activeSlide - 1, activeSlide + 2)

        setState({
            ...state,
            _slides,
            transition: 0,
            translate: getWidth()
        })
    }

    const nextSlide = () =>
        setState({
            ...state,
            translate: translate + getWidth(),
            activeSlide: activeSlide === slides.length - 1 ? 0 : activeSlide + 1
        }
        )

    const prevSlide = () =>
        setState({
            ...state,
            translate: 0,
            activeSlide: activeSlide === 0 ? slides.length - 1 : activeSlide - 1
        })

    return (
        <div css={SliderCSS} ref={sliderRef}>
            <SliderContent
                translate={translate}
                transition={transition}
                width={getWidth() * _slides.length}
            >
                {_slides.map((_slide, i) => (
                    <Slide width={getWidth()} key={_slide + i} content={_slide} />
                ))}
            </SliderContent>

            <Arrow direction="left" handleClick={prevSlide} />
            <Arrow direction="right" handleClick={nextSlide} />

            <Dots slides={slides} activeSlide={activeSlide} />
        </div>
    )
}

const SliderCSS = css`
  position: relative;
  height: 50%;
  width: 100%;
  margin: 0 auto;
  overflow: hidden;
  white-space: nowrap;
`

export default Slider