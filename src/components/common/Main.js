/** @jsxImportSource @emotion/react */
import React from 'react';
import { useIntl } from 'react-intl';
import { FaBars } from 'react-icons/fa';
import reactLogo from '../../logo.svg';
import Slider from "./Slider/Slider";
import images from "../../assets/images";


const Main = ({
                  handleToggleSidebar,
              }) => {
    const intl = useIntl();
    return (
        <main>
            <div className="btn-toggle" onClick={() => handleToggleSidebar(true)}>
                <FaBars />
            </div>
            <header>
                <h1>
                    <img width={80} src={reactLogo} alt="react logo" /> {intl.formatMessage({ id: 'title' })}
                </h1>
                <p>{intl.formatMessage({ id: 'description' })}</p>
            </header>


            <Slider slides={images} autoPlay={true}/>


            <footer>
                <small>
                    © 2021 made by Hugo BATT
                </small>
            </footer>
        </main>
    );
};
export default Main;
