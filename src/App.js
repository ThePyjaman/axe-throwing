import 'bootstrap/dist/css/bootstrap.min.css'
import Layout from "./components/common/Layout";
import './styles/App.scss';

function App() {
    return (
            <Layout/>
    );
}
export default App;
